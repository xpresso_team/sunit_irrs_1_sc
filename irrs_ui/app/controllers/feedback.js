var database = require('./database');
var jwt = require('jsonwebtoken');
var conf = require('../config');
var winston = require('../Loggerutil');
var normalizesql = require('./normalizesql').normalizesql;
const _ = require('lodash');

function addfeedback(req, res, next) {
  if (_.isNil(req.body.token) || _.isNil(req.body.rating) || _.isNil(req.body.feedback) || _.isNil(req.body.messagetext) || _.isNil(req.body.logid)){
    return res.sendStatus(400).status({
      "error": "Required field missing"
    });
  }
  let token = req.body.token;
  let rating = req.body.rating;
  let feedback = req.body.feedback;
  let messagetext = req.body.messagetext;
  var logid = req.body.logid;

  jwt.verify(token, conf.secret, function(err, decoded) {
    if (err) {
      res.sendStatus(503).send({
        "error": "Unable to decode token"
      });
    }
    let username = decoded.id;
    var query = "INSERT INTO feedback(logid,username,query,feedback,rating) VALUES ('" + logid + "','" + username + "','" + normalizesql(messagetext) + "','" + normalizesql(feedback) + "','" + rating + "');";
    winston.info("Query for adding to feedback table is : \n" + query);
    database.executeQuery(query, function(err, rows) {

      if (err) {
        winston.error("error in inserting the feedback into mysql is ", err);
        res.status(503).send({
          "error": "unable to add feedback to database"
        });
      }
      res.sendStatus(200);
    });
  });
}

function feedbackdata(req, res) {
    if(!("start" in req.body) ||  _.isNil(req.body.start) || !("end" in req.body) || _.isNil(req.body.end)){
      return res.sendStatus(400).status({
        "error": "Required field missing"
      });
    }
    var start = req.body.start;
    var end = req.body.end;
    var query = "select username,query,feedback,rating,api from feedback where date(time) >='" + start + "' and date(time) <='" + end + "' order by time desc;";
    database.executeQuery(query, function(err, data) {
      if (err) {
        console.log(err);
        res.status(503).send({
          "error": "unable to retrieve feedback data"
        });
        return;
      }
      res.status(200).send(data.rows)
    });
}


module.exports = {
  addfeedback: addfeedback,
  feedbackdata: feedbackdata
}
