var path = require('path');
var ejs = require('ejs');
var database = require('./database');
var winston = require('../Loggerutil');
const _ = require('lodash');

function gethomepage(req, res, next) {
  if (_.isNil(req.query.config)) {
    return res.send('<p>No congig parameter passed</p>');
  }
  var config = req.query.config;

  var base_query = 'SELECT dataset  as dataset from config';
  database.executeQuery(base_query, function(err, data) {
    if (err) {
      winston.error("error in querying database in gethomepage ", err);
      res.status(503).send({
        "error": "unable to query database"
      });
    }
    var possible_config = [];
    for (let i = 0; i < data.rows.length; i++) {
      possible_config.push(data.rows[i].dataset);
    }

    if (possible_config.indexOf(config) == -1) {
      return res.send('<p>Incorrect config parameter passed</p>');
    }

    var query = "SELECT columns as col, parameter as param from config where dataset='" + config + "';";
    winston.info("Query for retreieving column and parameter from config : \n" + query);

    database.executeQuery(query, function(err, data) {
      if (err) {
        winston.error("error in querying database in gethomepage ", err);
        res.status(503).send({
          "error": "unable to query database"

        });
      }

      if (_.isNil(data.rows[0].col) || _.isNil(data.rows[0].param)) {
        res.status(400).send({
          "error": "unable to find the required data in config"
        });
      }
      
      ejs.renderFile(path.join(__dirname, '../public/ejs/home.ejs'), {
        col: JSON.stringify(data.rows[0].col),
        param: JSON.parse(data.rows[0].param)
      }, function(err, str) {
        res.send(str);
      });
    });
  });
}

function getuserpage(req, res, next) {
  res.sendFile(path.join(__dirname, '../public/html/', 'usertable.html'));
}

module.exports = {
  getuserpage: getuserpage,
  gethomepage: gethomepage
}
